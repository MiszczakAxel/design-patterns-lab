package eu.telecomnancy.ui;

import eu.telecomnancy.Command;
import eu.telecomnancy.CommandeGetValue;
import eu.telecomnancy.CommandeOff;
import eu.telecomnancy.CommandeOn;
import eu.telecomnancy.CommandeUpdate;
import eu.telecomnancy.sensor.Arrondi;
import eu.telecomnancy.sensor.ConversionFahrenheit;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.MenuOptions;
import eu.telecomnancy.sensor.ProxyTemperatureSensor;
import eu.telecomnancy.sensor.SensorEtat;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class SensorView extends JPanel implements Observer {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ISensor sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton far = new JButton("Fahrenheit");
    private JButton arr = new JButton("Round");

    public SensorView(ISensor c) {
        this.sensor = c;
        Command allumer=new CommandeOn(this.sensor);
        Command eteindre=new CommandeOff(this.sensor);
        Command maj=new CommandeUpdate(this.sensor);
        Command avoirValeur=new CommandeGetValue(this.sensor);
        final MenuOptions controle=new MenuOptions(allumer,eteindre,maj,avoirValeur);
        if (this.sensor instanceof TemperatureSensor){
        	((TemperatureSensor) this.sensor).addObserver(this);
        }
        if (this.sensor instanceof SensorEtat){
        	((SensorEtat) this.sensor).addObserver(this);
        }
        if (this.sensor instanceof ProxyTemperatureSensor){
        	((ProxyTemperatureSensor) this.sensor).addObserver(this);
        }
        
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controle.allumer();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controle.eteindre();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    controle.mettreAJour();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        far.addActionListener(new ActionListener(){
        	 @Override
             public void actionPerformed(ActionEvent e) {
                 try {
                	 if (sensor instanceof TemperatureSensor){
                		 ConversionFahrenheit decosensor=new ConversionFahrenheit((TemperatureSensor)sensor);
                    	 decosensor.update();
                    	 value.setText(String.valueOf(decosensor.getValue())+"�F");
                	 } 
                 } catch (SensorNotActivatedException sensorNotActivatedException) {
                     sensorNotActivatedException.printStackTrace();
                 }
             }
        });
        arr.addActionListener(new ActionListener(){
       	 @Override
         public void actionPerformed(ActionEvent e) {
             try {
            	 if (sensor instanceof TemperatureSensor){
            		 Arrondi decosensor=new Arrondi((TemperatureSensor)sensor);
                	 decosensor.update();
                	 value.setText(String.valueOf(decosensor.getValue()));
            	 } 
             } catch (SensorNotActivatedException sensorNotActivatedException) {
                 sensorNotActivatedException.printStackTrace();
             }
         }
    });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 5));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(far);
        buttonsPanel.add(arr);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		ISensor sensor=(ISensor)arg0;
		try {
			this.value.setText(String.valueOf(sensor.getValue()));
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
		
	}
}
