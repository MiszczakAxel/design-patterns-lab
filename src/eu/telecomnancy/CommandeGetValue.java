package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeGetValue implements Command{
	private ISensor sensor;
	public CommandeGetValue(ISensor sensor){
		this.sensor=sensor;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		try {
			sensor.getValue();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
