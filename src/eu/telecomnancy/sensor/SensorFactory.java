package eu.telecomnancy.sensor;

import eu.telecomnancy.helpers.ReadPropertyFile;

import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 19:31
 */
public abstract class SensorFactory {
    public static ISensor makeSensor() {
        ReadPropertyFile rp = new ReadPropertyFile(); //cr�e un objet pouvant lire le .properties
        Properties p = null;
        try {
            p = rp.readFile("/eu/telecomnancy/app.properties"); //lit la sortie de la fct
        } catch (IOException e) {
            e.printStackTrace();
        }
        String factory = p.getProperty("factory"); //prend la description de l'item factory
        ISensor sensor;
        sensor = null;
        try {
            SensorFactory sensorfactory = (SensorFactory) Class.forName(factory).newInstance(); //cr�e une nouvelle instance ISensor
            sensor = sensorfactory.getSensor(); //prend le ISensor cr��

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return sensor;
    }

    public abstract ISensor getSensor();
}
