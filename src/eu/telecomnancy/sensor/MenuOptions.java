package eu.telecomnancy.sensor;

import eu.telecomnancy.Command;

public class MenuOptions {
	private Command allumerCommande;
	private Command eteindreCommande;
	private Command mettreAJourCommande;
	private Command lireValeurCommande;
	
	public MenuOptions(Command on, Command off, Command update, Command getValue){
		this.allumerCommande=on;
		this.eteindreCommande=off;
		this.mettreAJourCommande=update;
		this.lireValeurCommande=getValue;
	}
	public void allumer(){
		allumerCommande.execute();
	}
	public void eteindre(){
		eteindreCommande.execute();
	}
	public void mettreAJour() throws SensorNotActivatedException{
		mettreAJourCommande.execute();
	}
	public void lireValeur(){
		lireValeurCommande.execute();
	}
}
