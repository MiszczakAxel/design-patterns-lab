package eu.telecomnancy.sensor;

public class AdapterLegacy implements ISensor {
	private LegacyTemperatureSensor LTS;
	public AdapterLegacy(LegacyTemperatureSensor TS){
		this.LTS=TS;
	}
	public void on() {
		if (!this.LTS.getStatus()) LTS.onOff();
	}

	public void off() {
		if (this.LTS.getStatus()) LTS.onOff();
	}

	public boolean getStatus() {
		return this.LTS.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		if (this.LTS.getStatus()) this.LTS.getTemperature();
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	public double getValue() throws SensorNotActivatedException {
		if (this.LTS.getStatus()) return this.LTS.getTemperature();
		else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

}
