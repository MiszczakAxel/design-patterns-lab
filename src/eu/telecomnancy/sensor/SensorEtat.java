package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

public class SensorEtat extends Observable implements ISensor{
	
	public SensorState state;
	private double value;
	
	public SensorEtat(){
		state=new SensorOff();
		value=-1;
	}
	
	public class SensorOn implements SensorState{
		
		@Override
		public void on() {
			value=0;
		}

		@Override
		public void off() {
			state=new SensorOff();
		}

		@Override
		public boolean getStatus() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public void update(){
			// TODO Auto-generated method stub
	        	value = (new Random()).nextDouble() * 100;
	        	setChanged();
	        	notifyObservers();
		}

		@Override
		public double getValue() {
			// TODO Auto-generated method stub
			return value;
		}
		
	}
	public class SensorOff extends Observable implements SensorState{
		@Override
		public void on() {
			state=new SensorOn();
		}

		@Override
		public void off() {
			value=-1;
		}

		@Override
		public boolean getStatus() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void update() throws SensorNotActivatedException {
			// TODO Auto-generated method stub
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
		}

		@Override
		public double getValue() {
			// TODO Auto-generated method stub
			return -1;
		}
		
	}

	@Override
	public void on() {
		// TODO Auto-generated method stub
		state.on();
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		state.off();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		state.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return state.getValue();
	}

}
