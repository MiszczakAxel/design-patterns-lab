package eu.telecomnancy.sensor;

public class SensorFabrique {
	
	private ISensor sensor;
	public SensorFabrique(String type) throws Exception{
		if (type.toUpperCase().equals("TEMPERATURESENSOR"))
			sensor=new TemperatureSensor();
		else if (type.toUpperCase().equals("LEGACYTEMPERATURESENSOR"))
			sensor=new AdapterLegacy(new LegacyTemperatureSensor());
		else if (type.toUpperCase().equals("SENSORETAT"))
			sensor=new SensorEtat();
		else if (type.toUpperCase().equals("SENSORPROXY"))
			sensor=new ProxyTemperatureSensor();
		else throw new Exception("Ce capteur n'existe pas !");
	}
	public ISensor getSensor(){
		return sensor;
	}
}
