package eu.telecomnancy.sensor;

public class Arrondi extends DecoratedSensor{
	public Arrondi(TemperatureSensor sensor) {
		super(sensor);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void on() {
		// TODO Auto-generated method stub
		sensor.on();
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		sensor.off();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (sensor.getStatus()){
			sensor.value=Math.round(sensor.getValue());
		}
		else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (sensor.getStatus()) return sensor.getValue();
		else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}
}