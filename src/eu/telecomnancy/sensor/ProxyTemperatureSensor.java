package eu.telecomnancy.sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;

public class ProxyTemperatureSensor extends Observable implements ISensor{
	
	private TemperatureSensor sensor;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date();
	public ProxyTemperatureSensor(){
		sensor=new TemperatureSensor();
	}
	@Override
	public void on() {
		sensor.on();
		System.out.println(dateFormat.format(date)+"\n");
		System.out.println("M�thode appel�e : on()\n");
		System.out.println("Valeur de retour = aucune");
	}

	@Override
	public void off() {
		sensor.off();
		System.out.println(dateFormat.format(date)+"\n");
		System.out.println("M�thode appel�e : off()\n");
		System.out.println("Valeur de retour = aucune");
	}

	@Override
	public boolean getStatus() {
		boolean temp=sensor.getStatus();
		System.out.println(dateFormat.format(date)+"\n");
		System.out.println("M�thode appel�e : getStatus()\n");
		System.out.println("Valeur de retour = "+temp);
		return temp;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (sensor.getStatus()){
			sensor.update();
			setChanged();
        	notifyObservers();
			System.out.println(dateFormat.format(date)+"\n");
			System.out.println("M�thode appel�e : update()\n");
			System.out.println("Valeur de retour = "+sensor.getValue());
		}
		else {
			System.out.println(dateFormat.format(date)+"\n");
			System.out.println("M�thode appel�e : update()\n");
			System.out.println("Valeur de retour = exception SensorNotActivatedException");
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
		}
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (sensor.getStatus()){
			double temp=sensor.getValue();
			System.out.println(dateFormat.format(date)+"\n");
			System.out.println("M�thode appel�e : getStatus()\n");
			System.out.println("Valeur de retour = "+temp);
			return temp;
		}
		else {
			System.out.println(dateFormat.format(date)+"\n");
			System.out.println("M�thode appel�e : update()\n");
			System.out.println("Valeur de retour = exception SensorNotActivatedException");
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
		}
		
	}

}
