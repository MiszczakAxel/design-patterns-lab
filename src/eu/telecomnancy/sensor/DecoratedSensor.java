package eu.telecomnancy.sensor;

import java.util.Observable;

public abstract class DecoratedSensor extends Observable implements ISensor{
	protected TemperatureSensor sensor;
	public DecoratedSensor(TemperatureSensor sensor){
		this.sensor=sensor;
	}

}

