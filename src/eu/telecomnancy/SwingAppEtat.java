package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorEtat;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppEtat {
	public static void main(String[] args) throws Exception {
        ISensor sensor = new SensorEtat();
        new MainWindow(sensor);
    }
}
