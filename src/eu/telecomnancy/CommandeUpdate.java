package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeUpdate implements Command{
	private ISensor sensor;
	public CommandeUpdate(ISensor sensor){
		this.sensor=sensor;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
