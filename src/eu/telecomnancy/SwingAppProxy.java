package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ProxyTemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppProxy {
	public static void main(String[] args) throws Exception {
        ISensor sensor = new ProxyTemperatureSensor();
        new MainWindow(sensor);
    }
}
