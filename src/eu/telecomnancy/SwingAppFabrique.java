package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorFabrique;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppFabrique {
	public static void main(String[] args) throws Exception {
        ISensor sensor = new SensorFabrique("TemperatureSensor").getSensor();
        new MainWindow(sensor);
    }
}
